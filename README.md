> **NOTE:** This repo aims to be consistent with the [folder structure and workflow created by Patrick Ball at the Human Rights Data Analysis Group (HRDAG).](https://hrdag.org/2016/06/14/the-task-is-a-quantum-of-workflow/)


# Council Votes Chicago

## Purpose


As stated on the [Chi Hack Night breakout group page](https://github.com/chihacknight/breakout-groups/issues/189), the goals of this group are to present information about Chicago City Council's "Divided Roll Call" votes (non-unanimous votes) in ways that are human-friendly and in ways that are machine-friendly.


## Tasks


### Import the divided roll call votes data by scraping PDFs


Currently, we scrape the data from pdfs published by the [City Clerk's Council Meeting Reports page](http://www.chicityclerk.com/legislation-records/journals-and-reports/council-meeting-reports)

To access the pdfs: Filter by Subcategory > Select: Attendance/Divided Roll Call > Apply

Here is the [R script that imports and scrapes the data](https://gitlab.com/ipce_uic/council_votes/tree/master/tasks/import/src)

Here is the [R script that cleans the data](https://gitlab.com/ipce_uic/council_votes/tree/master/tasks/clean/src)

Here is the [R script that summarizes the data](https://gitlab.com/ipce_uic/council_votes/tree/master/tasks/summarize/src)

Here is the [working dataset for the 2019 session](https://gitlab.com/ipce_uic/council_votes/blob/master/tasks/summarize/output/Chicago_Council_Divided_Votes_2019_Session_All_SHARE.csv)

As is, to update this dataset, you must manually add unscraped files to [CCC_Divided_Roll_Call_links](https://gitlab.com/ipce_uic/council_votes/tree/master/tasks/import/hand). **An important subtask is to figure how to automate this process.**


### Import the divided roll call votes data from the Open Civic Data API


To get all of the aldermanic voting data (not just divided roll call votes) we'll need to access 
data from the Legistar API. 

Derek Eder [wrote a script](https://gist.github.com/derekeder/6250077831d93727c4c0b2051bd4df52) to get the divided roll call data from the [Open Civic Data API](https://ocd-api-documentation.readthedocs.io/en/latest/) that DataMade maintains.

We need to figure out how to use this API to get the same data published by the Clerk (Bill ID, Vote Date, Alderman Name, Alderman Vote) but for all votes.


### Share and visualize the data


This group has created [a Shiny dashboard](https://ipce.shinyapps.io/council_votes) that allows users to download the data and do some basic exploration.

Here is the [R script for the shiny app](https://gitlab.com/ipce_uic/council_votes/blob/master/tasks/share/Council_Votes/app.R)

Here are the features of the Shiny dashboard:

* **Visualize**
  * Parameters
    - [x] Select date range for the votes
    - [x] Select an Alderman to compare against
    - [ ] Select a type of bill (routine vs. non-routine, by subject)
  * Plot
    - [x] Group alderman into buckets based on % of the time they agree with the selected Alderman within the selected time period
  * Table
    - [x] Present Individual alderman and their % agreement with the selected Alderman within the selected time period

* **Data**
  * Parameters
    - [x] Select date range for the votes
    - [x] Select an Alderman to compare against
  * Table
    - [x] Bill ID with embedded link to its Councilmatic page
    - [x] Summary of Votes by Bill
    - [x] Votes by Bill for the selected Alderman within the selected time period

* **Extras**
    - [ ] Who is my alderman?
    - [ ] Group alderman by categories (caucus, demographics, geography, etc.)
  
 
